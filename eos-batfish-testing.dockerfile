FROM docker.io/ubuntu:18.04

# Make /data dir available to any user, so this container can be run by any user
RUN mkdir -p /data
RUN chmod a+rw /data
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
ENV JAVA_LIBRARY_PATH /usr/lib
ENV HOME /data

# Base package setup
RUN apt-get update && apt-get install -y \
    openjdk-8-jdk \
    git \
    maven \
    binutils \
    libgomp1 \
    lsb-release \
    openjdk-8-jre-headless \
    wget \
    zip \
    python3 \
    python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/oracle* \
    && pip3 install --upgrade pip

# build batfish
RUN git clone https://github.com/batfish/batfish.git
RUN batfish/tools/install_z3.sh
RUN cd batfish && /bin/bash -c "source tools/batfish_functions.sh && batfish_build_all" && cd /

# python
RUN pip3 install pybatfish \
    attrdict \
    jupyter \
    matplotlib \
    networkx \
    ansible \
    ipaddr \
    exitstatus \
    netaddr \
    pybatfish

# Batfish
ENTRYPOINT /bin/bash -c 'cd /batfish/ && source tools/batfish_functions.sh && allinone -runclient false &' && bash

