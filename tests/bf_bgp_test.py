#!/usr/bin/env python3

from ansible.inventory.manager import InventoryManager
from ansible.vars.manager import VariableManager
from ansible.parsing.dataloader import DataLoader
from pybatfish.client.commands import *
from pybatfish.question.question import load_questions, list_questions
from pybatfish.question import bfq
from ipaddr import IPNetwork
from exitstatus import ExitStatus
import sys

test_passed = True

# Takes care of finding and reading yaml, json and ini files
loader = DataLoader()

# create inventory, use path to host config file as source or hosts in a comma separated string
inventory = InventoryManager(loader=loader, sources='batfish_inventory.ini')

# variable manager takes care of merging all the different sources to give you a unified view of variables available in each context
variable_manager = VariableManager(loader=loader, inventory=inventory)

# Collect list of spine switches
spines = variable_manager.get_vars()['groups']['clos_spine']

# convert str list to obj representing hosts. can get str name with .get_name()
spines = [inventory.get_host(spine) for spine in spines]

# get all the spine->leaf links defined in ansible - don't need to get all the leaf-ends of this because we'll test each bgp session status
underlay_bgp_links = []
for spine in spines:
    underlay_bgp_links += variable_manager.get_vars(include_hostvars=True, host=spine)['downlinks']

# pare this down to just the IP addresses of linknets
underlay_bgp_linknets = [link['linknet'] for link in underlay_bgp_links]

# get supported questions from batfish and init snapshot of configs generated by ansible
bf_session.host = 'localhost'
load_questions()
bf_init_snapshot("bf_snapshot")

# get all bgp sessions details from snapshot including expected session status
fabric_bgp_status = bfq.bgpSessionStatus().answer().frame()
ip_owners = bfq.ipOwners().answer().frame()

for linknet in underlay_bgp_linknets:
    str_linknet = str(IPNetwork(linknet).ip)
    # for each linknet from a spine, pick out any BGP sessions using the first IP in that linknet
    test_bgp_instance = fabric_bgp_status[(fabric_bgp_status.Local_IP==str_linknet)]
    # if no sessions exist at all with that IP, that's weird and bad
    if len(test_bgp_instance) < 1:
        print(f"Linknet {str_linknet} configured for spine but not detected in Batfish Snapshot - this doesn't make sense")
        peer_session = fabric_bgp_status[(fabric_bgp_status.Local_IP==str(IPNetwork(linknet).ip+1))]
        if len(peer_session) > 0:
            print(f"BGP session from peer IP {peer_session.iloc[0]['Local_IP']} on node {peer_session.iloc[0]['Node']} exists but is {peer_session.iloc[0]['Established_Status']}")
        test_passed = False
    # If more than one session using that IP exists, something is misconfigured!
    elif len(test_bgp_instance) > 1:
        duplicate_link_nodes = [test_bgp_instance.iloc()[index]["Remote_Node"] for index in range(len(test_bgp_instance))]
        print(f"spine linknet {str_linknet} used in multiple places! Remove it from one of the links to nodes {duplicate_link_nodes}")
        test_passed = False
    # If a session exists but is not ESTABLISHED this likely means something has gone wrong in the spine or leaf config
    elif test_bgp_instance.iloc()[0]["Established_Status"] != "ESTABLISHED":
        failed_node = test_bgp_instance.iloc()[0]['Remote_Node']
        print(f"{str_linknet} bgp session not established to {failed_node}")
        test_passed = False

# find likely misconfigured sessions
incompatible_nodes = fabric_bgp_status[(fabric_bgp_status.Established_Status=="NOT_COMPATIBLE")]
if len(incompatible_nodes) > 0:
    farend_failures = [incompatible_nodes['Remote_IP'].iloc()[index] for index in range(len(incompatible_nodes))]
    local_failures = [incompatible_nodes['Node'].iloc()[index] for index in range(len(incompatible_nodes))]
    for index in range(len(farend_failures)):
        ip_owners[(ip_owners.IP==farend_failures[index])]
        print(f"Node {local_failures[index]} cannot establish a BGP session to {farend_failures[index]}, which is probably on node {ip_owners[(ip_owners.IP==farend_failures[index])].iloc()[0]['Node']}")
    test_passed = False

if test_passed:
    sys.exit(ExitStatus.success)
else:
    sys.exit(ExitStatus.failure)
