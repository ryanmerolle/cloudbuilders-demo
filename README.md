# Cloudbuilders 2019 demo by Alex White-Robinson

##  Get a cEOS image, 4.22.0 or higher  
`Requires an account` - https://www.arista.com/en/support/software-download


![](images/ceos_download.png)


## Get docker & docker-compose
```
$ curl -fsSL https://get.docker.com -o get-docker.sh  
$ sh get-docker.sh  
$ systemctl enable docker && systemctl start docker  
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```

## Get ansible & required packages:  
```
$ dnf install python3-netaddr ansible 
$ pip3 install docker
```

## Import your image  
```
$ docker import ~/Downloads/cEOS-lab-4.22.0.1F.tar.xz ceosimage:4.22.0.1F
```

## Stand up the environment  
```
$ docker-compose up -d
```

## Wait a few minutes until containers finish building / standing up  

![](images/coffee.png)


##  Configure a network environment - modify group vars, templates & tasks to suit!  
```
$ ansible-playbook ceos_config_push.yaml -i inventory-docker.py -u admin -k
SSH password: admin
```

## Jump into your L3LS and validate things are working
```
$ docker exec -it test-c99-f0102-ltr01a Cli
test-c99-f0102-ltr01a>en
test-c99-f0102-ltr01a#show ip bgp summary
BGP summary information for VRF default
Router identifier 10.168.63.64, local AS number 65001
Neighbor Status Codes: m - Under maintenance
  Neighbor         V  AS           MsgRcvd   MsgSent  InQ OutQ  Up/Down State   PfxRcd PfxAcc
  10.168.60.0      4  65000             10         9    0    0 00:01:08 Estab   11     11
  10.168.62.0      4  65000              8         7    0    0 00:01:08 Estab   11     11

test-c99-f0102-ltr01a#show ip route 10.168.63.129

VRF: default
Codes: C - connected, S - static, K - kernel,
       O - OSPF, IA - OSPF inter area, E1 - OSPF external type 1,
       E2 - OSPF external type 2, N1 - OSPF NSSA external type 1,
       N2 - OSPF NSSA external type2, B I - iBGP, B E - eBGP,
       R - RIP, I L1 - IS-IS level 1, I L2 - IS-IS level 2,
       O3 - OSPFv3, A B - BGP Aggregate, A O - OSPF Summary,
       NG - Nexthop Group Static Route, V - VXLAN Control Service,
       DH - DHCP client installed default route, M - Martian,
       DP - Dynamic Policy Route, L - VRF Leaked

 B E      10.168.63.129/32 [20/0] via 10.168.60.0, Ethernet1
                                  via 10.168.62.0, Ethernet2

test-c99-f0102-ltr01a#show bgp evpn summary
BGP summary information for VRF default
Router identifier 10.168.63.64, local AS number 65001
Neighbor Status Codes: m - Under maintenance
  Neighbor         V  AS           MsgRcvd   MsgSent  InQ OutQ  Up/Down State   PfxRcd PfxAcc
  10.168.63.0      4  65000             12        12    0    0 00:00:10 Estab   12     12
  10.168.63.2      4  65000             13         9    0    0 00:01:41 Estab   12     12

test-c99-f0102-ltr01a#show bgp evpn vni 10280
BGP routing table information for VRF default
Router identifier 10.168.63.64, local AS number 65001
Route status codes: s - suppressed, * - valid, > - active, # - not installed, E - ECMP head, e - ECMP
                    S - Stale, c - Contributing to ECMP, b - backup
                    % - Pending BGP convergence
Origin codes: i - IGP, e - EGP, ? - incomplete
AS Path Attributes: Or-ID - Originator ID, C-LST - Cluster List, LL Nexthop - Link Local Nexthop

         Network                Next Hop            Metric  LocPref Weight  Path
 * >     RD: 10.168.63.64:10280 imet 10.168.63.128
                                -                     -       -       0        i
         RD: 10.168.63.65:10280 imet 10.168.63.128
                                10.168.63.128         -       100     0       65000 65001 i
         RD: 10.168.63.65:10280 imet 10.168.63.128
                                10.168.63.128         -       100     0       65000 65001 i
 * >Ec   RD: 10.168.63.66:10280 imet 10.168.63.129
                                10.168.63.129         -       100     0       65000 65001 i
 *  ec   RD: 10.168.63.66:10280 imet 10.168.63.129
                                10.168.63.129         -       100     0       65000 65001 i
 * >Ec   RD: 10.168.63.67:10280 imet 10.168.63.129
                                10.168.63.129         -       100     0       65000 65001 i
 *  ec   RD: 10.168.63.67:10280 imet 10.168.63.129
                                10.168.63.129         -       100     0       65000 65001 i
```

## Support & contributions  
Questions? Suggestions? Raise an issue, or email me @ alex.white-robinson@concepts.co.nz.


## Credits  
* **Project** https://github.com/rywillia/docker-inventory  
  **Copyright** 2017 Ryan Williams  
  **License** (GPLv3) https://github.com/rywillia/docker-inventory/blob/master/LICENSE  
