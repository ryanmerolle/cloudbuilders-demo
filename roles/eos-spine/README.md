# Ansible Role: eos-spine

This role configures spine switches 

## Requirements

python3-netaddr (can also be installed with `pip3 install netaddr`

## Role Variables

`leaf_asn` and `spine_asn` are set in the site-specific fabric group, such as `c99_fabric`.  
e.g. `spine_asn: 65000` and `leaf_asn: 65001`

`router_id` is typically set in the host_vars file and relies on the `loopback0.ip` value  
e.g. `router_id: "{{loopback0.ip | ipaddr('address')}}"`

`bgp_password` is used for bgp authentication.

`loopback0` is used for EVPN peering, and is typically set in host_vars.
e.g.
`
loopback0:
  ip: 10.168.63.72/32
`

`downlinks` is a list of connections from the spine to the leaves. Underlay links are defined based on these entries.  
Control-plane ACLs utilize these entries to permit BGP from appropriate IP ranges. This is typically defined in host_vars.

e.g.
`
downlinks:
  - { interface: ethernet27/1, linknet: 10.168.61.4/31, neighbor: test-c99-f0103-lyc01a, neighbor_int: eth50/1, speed: 100g }
  - { interface: ethernet28/1, linknet: 10.168.61.6/31, neighbor: test-c99-f1603-lyc01b, neighbor_int: eth50/1, speed: 100g }
  - { interface: ethernet1/4, linknet: 10.168.61.10/31, neighbor: test-c99-f0102-ltr02a, neighbor_int: eth50, speed: 10000 }
  - { interface: ethernet2/4, linknet: 10.168.61.14/31, neighbor: test-c99-f1602-ltr02b, neighbor_int: eth50, speed: 10000 }
`

`linknet_summary` is the spine's allocated IP range for P2P links to leaves. This IP range is used for dynamic peerings on spines and is defined in spines' host_vars.  
e.g.
`linknet_summary: 10.168.61.0/24` is rendered into in `bgp listen range {{linknet_summary}} peer-group UNDERLAY remote-as {{leaf_asn}}`

`leaf_evpn_loopbacks` is used in a similar way to `linknet_summary`, but is the IP range used for EVPN peering to leaves rather than physical interfaces.  
Because this range, unlike `linknet_summary`, is identical in use for all spines it is defined in the site specific fabric group such as `c99_fabric`.  
e.g.
`leaf_evpn_loopbacks: 10.168.63.64/26`

## Tasks

### main.yml

This task sets up the folder for configuration fragments to be placed in, assembles them into a single configuration file and pushes them to the switch.  
The eos-common role is included in this playbook.  
Once configuration has been pushed, it saves the running-config to startup-config.  
When this task has executed it will delete the configuration folder, fragments and the final configuration file for the host it executed against.
